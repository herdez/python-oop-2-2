# Challenge OOP-2

`xantecy, S.A.P.I. de C.V.` has the following problem:


```python
class Person:
    def set_name(self, name):
        self.name = name
    def set_job(self, job):
        self.job = job
```


```python
charly = Person()
```

If we try to access an attribute before setting it we'll get an error.


```python
charly.name
```


    ---------------------------------------------------------------------------

    AttributeError                            Traceback (most recent call last)

    <ipython-input-3-b123a67a06c2> in <module>()
    ----> 1 charly.name
    

    AttributeError: 'Person' object has no attribute 'name'



```python
charly.set_name('Charly')
```


```python
charly.name

#>>>'Charly'
```

To avoid errors such as this, where other methods are called to populate these fields (attributes), so define the `Person` class to avoid it:

```python
"""Person Class"""

...

```


```python
"""driver code"""

print(charly.name == 'Charly')
print(charly.job == 'Dev')
```
